#pragma once
#include <cstdint>
#include "data/data.hpp"

bool showStructureGui(int32_t structureId, int32_t structureTypeId, GameData& data);
